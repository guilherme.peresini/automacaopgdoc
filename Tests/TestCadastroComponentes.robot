*** Settings ***
Resource             ../resource/Resource.robot
Test SETUP           Abrir navegador
Test TEARDOWN        Fechar navegador

*** Variables ***
#LOCATORS TELA CLIENTES
${BOTAO_INCLUIR}     xpath=//form[@id='lookupForm']//a[contains(text()," Inserir")]

#LOCATORS TELA NOVO COMPONENTE
${INPUT_NOME}        id=Data_Name
${BOTAO_CANCELAR}    xpath=//form[@id='mainForm']//a[contains(text()," Cancelar")]
${BOTAO_VOLTAR}      xpath=//a[contains(text()," Voltar")]
${BOTAO_SALVAR}      xpath=//*[@id='submitButton']
${CHECK_CONFIRM}     xpath=//*[@id="confirmation"]/../label
${EXCLUIR}           id=submitButton

#LOCATORS TELA DETALHES COMPONENTE
${BOTAO_EDITAR}      xpath=//a[@title='Editar']
${BOTAO_EXCLUIR}     xpath=//a[@title='Excluir']
${VOLTAR}            xpath=//a[@title='Voltar']

*** Test Cases ***
Cenário 01: Validação de voltar na tela de novo componente
    Dado que estou na tela de Novo componente
    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Componentes"

Cenário 02: Validação do cancelar na tela de novo componente
    Dado que estou na tela de Novo componente
    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Componentes"

Cenário 03: Validação de cadastro de componentes sem informar o nome
    Dado que estou na tela de Novo componente
    Quando clicar em "Salvar"
    Então deve ser exibido uma validação do formulário

Cenário 04: Validação de cadastro de novo componente
    Dado que estou na tela de Novo componente
    Quando preencher o formulário e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do componente"

Cenário 06: Validação de acesso à Editar componente através da página Detalhes do componente
    Dado que estou na tela Detalhes do componente
    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar componente"

Cenário 07: Validação de acesso à Excluir componente através da página Detalhes do componente
    Dado que estou na tela Detalhes do componente
    Quando clicar em "Excluir"
    Então deve ser exibida a tela "Excluir componente"

Cenário 08: Validação do voltar na tela Detalhes do componente
    Dado que estou na tela Detalhes do componente
    Quando clicar em " Voltar"
    Então deve ser exibida a tela "Componentes"

Cenário 09: Validação de acesso à página Detalhes componente pela lista de componentes
    Dado que estou na tela Componentes
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Editar cliente"

Cenário 10: Validação de acesso à página Editar componente pela lista de componentes
    Dado que estou na tela Componentes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

Cenário 11: Validação de acesso à página Excluir componente pela lista de componentes
    Dado que estou na tela Componentes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

Cenário 12: Validação do voltar na tela Editar componente
    Dado que estou na tela Componentes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do componente"

Cenário 13: Validação do cancelar na tela Editar componente
    Dado que estou na tela Componentes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Detalhes do componente"

Cenário 14: Validação de alteração de cadastro de componente sem informar o campo nome
    Dado que estou na tela Componentes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando clicar em salvar sem o campo nome preenchido
    Então deve ser exibido uma validação do formulário

Cenário 15: Validação de alteração de cadastro de componente
    Dado que estou na tela Componentes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando alterar o nome do componente e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 16: Validação do voltar na tela Excluir template
    Dado que estou na tela Componentes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do componente"

Cenário 17: Validação do cancelar na tela Excluir template
    Dado que estou na tela Componentes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Detalhes do componente"

Cenário 18: Validação de exclusão de cadastro de componente
    Dado que estou na tela Componentes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

    Quando marcar o check e clicar em Excluir
    Então deve ser exibida a tela "Componentes"


*** Keywords ***
Dado que estou na tela de Novo componente
    Realizar login                                            ${LOGIN.username}                                        ${LOGIN.senha}
    Sleep                                                     2s
    Acessar menu                                              Componentes
    Sleep                                                     2s
    Click Link                                                ${BOTAO_INCLUIR}
    Wait Until Page Contains Element                          xpath=//li[contains(text(),"Novo componente")]           10s

Dado que estou na tela Componentes
    Realizar login                                            ${LOGIN.username}                                        ${LOGIN.senha}
    Sleep                                                     2s
    Acessar menu                                              Componentes

Dado que estou na tela Detalhes do componente
    Realizar login                                            ${LOGIN.username}                                        ${LOGIN.senha}
    Sleep                                                     2s
    Acessar menu                                              Componentes
    Sleep                                                     2s
    Click Link                                                xpath=//a[contains(text()," Teste Automação QA")]
    Wait Until Page Contains Element                          xpath=//li[contains(text(),"Detalhes do componente")]    10s

Quando marcar o check e clicar em Excluir
    Sleep                                                     2s
    Click Element                                             ${CHECK_CONFIRM}
    Click Element                                             ${EXCLUIR}

Quando alterar o nome do componente e clicar em Salvar
    Sleep                                                     2s
    Input Text                                                ${INPUT_NOME}                                            Teste Automação QA alterado
    Click Element                                             ${BOTAO_SALVAR}


Quando clicar em salvar sem o campo nome preenchido
    Sleep                                                     2s
    Clear Element Text                                        ${INPUT_NOME}
    Click Element                                             ${BOTAO_SALVAR}

Quando preencher o formulário e clicar em Salvar
    Sleep                                                     2s
    Input Text                                                ${INPUT_NOME}                                            Teste Automação QA
    Click Element                                             ${BOTAO_SALVAR}

Quando clicar em "${ELEMENTO}"
    Run Keyword If                                            '${ELEMENTO}'=='Cancelar'                                Run Keywords                           Sleep                                                                                                    2s                                        AND    Click Link    ${BOTAO_CANCELAR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='Salvar'                Run Keywords                                                                                             Sleep                                     2s     AND           Click Element        ${BOTAO_SALVAR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='Editar'                Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EDITAR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='Excluir'               Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EXCLUIR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='Voltar'                Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_VOLTAR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'==' Voltar'               Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${VOLTAR}
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='Icone com lápis'       Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Editar"]
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='lixeira'               Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Excluir"]
    ...                                                       ELSE IF                                                  '${ELEMENTO}'=='icone I'               Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Detalhes"]

Então deve ser exibida a tela "${TELA}"
    Run Keyword If                                            '${TELA}'=='Componentes'                                 Page Should Contain Element            xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${TELA}")]
    ...                                                       ELSE IF                                                  '${TELA}'=='Detalhes do componente'    Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                       ELSE IF                                                  '${TELA}'=='Editar componente'         Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                       ELSE IF                                                  '${TELA}'=='Excluir componente'        Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]

Então deve ser exibido uma validação do formulário
    Element Should Be Visible                                 id=Data_Name-error