*** Settings ***
Resource              ../resource/Resource.robot
Test SETUP            Abrir navegador
Test TEARDOWN         Fechar navegador

*** Variables ***
#LOCATORS LAYOUT DA TEMPLATE
${GUIA_LAYOUTS}       xpath=//a[contains(text()," Layouts")]
${BOTAO_INCLUiR}      xpath=//a[contains(text()," Incluir")]

#LOCATORS NOVO LAYOUT
${VOLTAR}             xpath=//a[contains(text()," Voltar")]
${NORMAL}             xpath=//a[contains(text()," Normal")]
${CHECK_COPIAR}       xpath=//label[contains(text(),"Copiar dados de layout existente")]

#LOCATORS NOVO LAYOUT (NORMAL)
${CANCELAR}           xpath=//a[contains(text()," Cancelar")]
${INPUT_DESCRICAO}    id=Data_Description
${SALVAR}             id=submitButton

#LOCATORS EXCLUIR LAYOUT
${CHECKBOX}           xpath=//label[contains(text(),"Sim, eu realmente quero excluir este layout.")]
${CANCELAR}           xpath=//a[contains(text()," Cancelar")]
${EXCLUIR}            id=submitButton

#LOCATORS DETALHES DO LAYOUT
${GUIA_DADOS}         xpath=//a[@title='Dados de Exemplo']
${EDITOR_VISUAL}      xpath=//span[contains(text(),"Editor Visual")]/..
${ENCERRAR_EDICAO}    xpath=//a[@title='Encerrar Edição']
${PUBLICAR}           xpath=//a[contains(text()," Publicar")]

#LOCATORS DADOS DO EXEMPLO
${EDITAR}             xpath=//a[contains(text()," Editar")]
${SALVAR}             id=submitButton

#LOCATORS ENCERRA EDICAO
${LABEL}              xpath=//label[contains(text(),"Sim, eu realmente quero encerrar a edição deste layout.")]

#LOCATORS PUBLIC LAYOUT
${RADIO_HMLG}         id=Data_AmbianceId
${MOTIVO}             id=Data_Description

*** Test Cases ***
Cenário 01: Validação de acesso à pagina Layouts da Template
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

Cenário 02: Validação do voltar tela novo layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "Incluir"
    Então deve ser exibida a tela "Novo Layout"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Layouts da Template"

Cenário 03: Validação do cancelar cadastro de novo layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "Incluir"
    Então deve ser exibida a tela "Novo Layout"

    Quando clicar em "Normal"
    Então deve ser exibida a tela "Novo Layout (Normal)"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Novo Layout"

Cenário 04: Validação de criação de layout sem copiar existente
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "Incluir"
    Então deve ser exibida a tela "Novo Layout"

    Quando clicar em "Normal"
    Então deve ser exibida a tela "Novo Layout (Normal)"

    Quando desmarcar o check e preencher o campo descrição e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 05: Validação do voltar tela Editar dados de exemplo
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Dados"
    Então deve ser exibida a tela "Dados de exemplo"

    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar dados de exemplo"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Dados de exemplo"

Cenário 06: Validação do cancelar Editar dados de exemplo
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Dados"
    Então deve ser exibida a tela "Dados de exemplo"

    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar dados de exemplo"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Dados de exemplo"

Cenário 07: Validação de inserção de dados de exemplo
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Dados"
    Então deve ser exibida a tela "Dados de exemplo"

    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar dados de exemplo"

    Quando clicar em "Salvar"
    Então deve ser exibida a tela "Dados de exemplo"


Cenário 08: Validação de edição de layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar layout"

    Quando alterar a descrição e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 09: Validação de cancelamente de exclusão de layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir Layout"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 10: Validação de exclusão de layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir Layout"

    Quando marcar o checkbox e clicar em excluir
    Então deve ser exibida a tela "Layouts da Template"

Cenário 11: Validação de criação de layout copiando existente
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "Incluir"
    Então deve ser exibida a tela "Novo Layout"

    Quando clicar em "Normal"
    Então deve ser exibida a tela "Novo Layout (Normal)"

    Quando preencher o campo descrição e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do Layout"

# Cenário 10: Validação de edição de visual
#                     Dado que estou na tela Templates
#                     Quando clicar em "icone I"
#                     Então deve ser exibida a tela "Detalhes da template"

#                     Quando clicar na guia "Layouts"
#                     Então deve ser exibida a tela "Layouts da Template"

#                     Quando clicar em "icone I2"
#                     Então deve ser exibida a tela "Detalhes do Layout"

#                     Quando clicar em Editor Visual

Cenário 12: Validação do voltar na tela Encerrar edição
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Encerrar Edição"
    Então deve ser exibida a tela "Encerrar edição"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 13: Validação do cancelar Encerrar edição
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Encerrar Edição"
    Então deve ser exibida a tela "Encerrar edição"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 14: Validação de encerrar edição
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Encerrar Edição"
    Então deve ser exibida a tela "Encerrar edição"

    Quando marcar o checkbox e clicar em Confirmar
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 15: Validação do voltar na tela de Publicar Layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Publicar"
    Então deve ser exibida a tela "Publicar Layout"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 16: Validação do cancelar na tela de Publicar Layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Publicar"
    Então deve ser exibida a tela "Publicar Layout"

    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Detalhes do Layout"

Cenário 17: Validação de publicação de layout
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

    Quando clicar na guia "Layouts"
    Então deve ser exibida a tela "Layouts da Template"

    Quando clicar em "icone I2"
    Então deve ser exibida a tela "Detalhes do Layout"

    Quando clicar em "Publicar"
    Então deve ser exibida a tela "Publicar Layout"

    Quando marcar o radio Homologação, descrever o motivo e publicar
    Então deve ser exibida a tela "Detalhes do Layout"

*** Keywords ***
Dado que estou na tela Templates
    Realizar login                                                               ${LOGIN.username}            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Templates

Quando marcar o radio Homologação, descrever o motivo e publicar
    Sleep                                                                        2s
    Click Element                                                                ${RADIO_HMLG}
    Input Text                                                                   ${MOTIVO}                    Teste Automação QA
    Click Element                                                                ${SALVAR}

Quando marcar o checkbox e clicar em Confirmar
    Sleep                                                                        2s
    Click Element                                                                ${LABEL}
    Click Element                                                                ${SALVAR}

Quando clicar em "${ELEMENTO}"
    Run Keyword If                                                               '${ELEMENTO}'=='Cancelar'    Run Keywords                        Sleep                                                                                                    2s                                        AND    Click Link    ${CANCELAR}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Incluir'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${BOTAO_INCLUIR}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Normal'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${NORMAL}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Salvar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Element      ${SALVAR}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Encerrar Edição'    Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${ENCERRAR_EDICAO}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Publicar'           Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${PUBLICAR}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Voltar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${VOLTAR}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Icone com lápis'    Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         xpath=//span[contains(text(),"Teste Automação QA")]/../../td/a[@title="Editar"]
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='lixeira'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         xpath=//span[contains(text(),"Teste Automação QA")]/../../td/a[@title="Excluir"]
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='icone I'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         xpath=//a[contains(text()," Utilizado na automação")]/../../td/a[@title="Detalhes"]
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='icone I2'           Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         xpath=//span[contains(text(),"Teste Automação QA")]/../../td/a[@title="Detalhes"]
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Dados'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${GUIA_DADOS}
    ...                                                                          ELSE IF                      '${ELEMENTO}'=='Editar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link         ${EDITAR}

Quando clicar na guia "${GUIA}"
    Run Keyword If                                                               '${GUIA}'=='Layouts'         Run Keywords                        Sleep                                                                                                    2s                                        AND    Click Link    ${GUIA_LAYOUTS}

Quando preencher o campo descrição e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_DESCRICAO}           Teste Automação QA
    Click Button                                                                 ${SALVAR}

Quando desmarcar o check e preencher o campo descrição e clicar em Salvar
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_COPIAR}
    Input Text                                                                   ${INPUT_DESCRICAO}           Teste Automação QA
    Click Button                                                                 ${SALVAR}

Quando alterar a descrição e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_DESCRICAO}           Teste Automação QA alterada
    Click Button                                                                 ${SALVAR}

Quando marcar o checkbox e clicar em excluir
    Sleep                                                                        2s
    Click Element                                                                ${CHECKBOX}
    Click Element                                                                ${EXCLUIR}

Então deve ser exibida a tela "${TELA}"
    Run Keyword If                                                               '${TELA}'=='Templates'       Page Should Contain Element         xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                      '${TELA}'!='Templates'              Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
