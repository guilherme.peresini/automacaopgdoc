*** Settings ***
Resource                ../resource/Resource.robot
Test SETUP              Abrir navegador
Test TEARDOWN           Fechar navegador

*** Variables ***
#LOCATORS TELA TEMPLATES
${BOTAO_INCLUIR}        xpath=//form[@id='lookupForm']//a[contains(text()," Incluir")]
${BOTAO_ATUALIZAR}      xpath=//form[@id='lookupForm']//button
${INPUT_FILTRO}         id=filter

#LOCATORS TELA NOVO TEMPLATE
${INPUT_NOME}           id=Data_Name
${SELECT_CLIENTE}       id=Data_CustomerId
${BOTAO_CANCELAR}       xpath=//form[@id='mainForm']//a[contains(text()," Cancelar")]
${BOTAO_SALVAR}         xpath=//*[@id='submitButton']
${CHECK_SITUACAO}       xpath=//*[@id="Data_IsActive"]/../label

#LOCATORS TELA DETALHES TEMPLATE
${BOTAO_EDITAR}         xpath=//a[contains(text(),"Editar")]
${BOTAO_EXCLUIR}        xpath=//a[contains(text(),"Excluir")]
${BOTAO_VOLTAR}         xpath=//a[contains(text(),"Voltar")]

#LOCATORS TELA ExCLUIR TEMPLATE
${CHECK_CONFIRM}        xpath=//*[@id="confirmation"]/../label
${EXCLUIR_CLIENTE}      id=submitButton
${CANCELAR_EXCLUSAO}    xpath=//*[@id='submitButton']/../a

*** Test Cases ***
Cenário 01: Validação de cancelamento de cadastro de template
    Dado que estou na tela de Novo Template
    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Templates"

Cenário 02: Validação de gravação de template sem preencher o formulário
    Dado que estou na tela de Novo Template
    Quando clicar em "Salvar"
    Então deve ser exibido validações do formulário

Cenário 03: Validação de gravação de template sem preencher o campo nome
    Dado que estou na tela de Novo Template
    Quando preencher o formulário sem informar um nome e clicar em Salvar
    Então deve ser exibido a validação de "nome"

Cenário 04: Validação de gravação de template sem preencher o campo cliente
    Dado que estou na tela de Novo Template
    Quando preencher o formulário sem informar um cliente e clicar em Salvar
    Então deve ser exibido a validação de "cliente"

Cenário 05: Validação de gravação de template
    Dado que estou na tela de Novo Template
    Quando preencher todo formulário e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 06: Validação de acesso à edição de template pela página Detalhes da template
    Dado que estou na tela de Detalhes da template
    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar template"

Cenário 07: Validação de acesso à exclusão de template pela página Detalhes da template
    Dado que estou na tela de Detalhes da template
    Quando clicar em "Excluir"
    Então deve ser exibida a tela "Excluir template"

Cenário 08: Validação de acesso à Detalhes do cliente pela página Detalhes da template
    Dado que estou na tela de Detalhes da template
    Quando clicar em "cliente"
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 09: Validação de voltar pela página Detalhes da template
    Dado que estou na tela de Detalhes da template
    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Templates"

Cenário 10: Validação de acesso à edição de template pela página Templates
    Dado que estou na tela Templates
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

Cenário 11: Validação de acesso à exclusão de template pela página Templates
    Dado que estou na tela Templates
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir template"

Cenário 12: Validação de acesso à Detalhes do template pela página Templates
    Dado que estou na tela Templates
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes da template"

Cenário 13: Validação de edição de template com nome vazio
    Dado que estou na tela Templates
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

    Quando clicar em Salvar com o campo nome vazio
    Então deve ser exibido a validação de "nome"

Cenário 14: Validação de edição de template sem cliente
    Dado que estou na tela Templates
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

    Quando clicar em Salvar com o campo cliente vazio
    Então deve ser exibido a validação de "cliente"

Cenário 15: Validação de edição de template
    Dado que estou na tela Templates
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

    Quando alterar o nome do template e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 16: Validação de cancelamento de exclusão de cadastro de template
    Dado que estou na tela Templates
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir template"

    Quando clicar em "X Cancelar"
    Então deve ser exibida a tela "Detalhes da template"

Cenário 17: Validação de bloqueio de template
    Dado que estou na tela Templates
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

    Quando desmarcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 18: Validação de desbloqueio de template
    Dado que estou na tela Templates
    Quando filtrar por Inativo e clicar em Atualizar
    Então deve ser exibido o template inativo

    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar template"

    Quando marcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 19: Validação de exclusão de cadastro de template
    Dado que estou na tela Templates
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir template"

    Quando marcar o check e clicar em Excluir
    Então deve ser exibida a tela "Templates"

Cenário 20: Validação de exclusão de template com layout publicado
    Dado que estou na tela Templates
    Quando clicar na lixeira de um template que tem layout publicado
    Então deve ser exibida uma mensagem informando que não é possível excluir

*** Keywords ***
Dado que estou na tela de Novo Template
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Templates
    Sleep                                                                        2s
    Click Link                                                                   ${BOTAO_INCLUIR}
    Wait Until Page Contains Element                                             xpath=//li[contains(text(),"Nova Template")]                                                 10s

Dado que estou na tela de Detalhes da template
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Templates
    Sleep                                                                        2s
    Click Link                                                                   xpath=//a[contains(text()," Teste Automação QA")]
    Wait Until Page Contains Element                                             xpath=//li[contains(text(),"Detalhes da template")]                                          10s

Dado que estou na tela Templates
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Templates

Quando clicar em "${ELEMENTO}"
    Run Keyword If                                                               '${ELEMENTO}'=='Cancelar'                                                                    Run Keywords                         Sleep                                                                                                    2s                                        AND    Click Link    ${BOTAO_CANCELAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Salvar'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Element        ${BOTAO_SALVAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Editar'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EDITAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Excluir'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EXCLUIR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Voltar'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_VOLTAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='cliente'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text(),"Utilizado na automação")]
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Icone com lápis'     Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Editar"]
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='lixeira'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Excluir"]
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='X Cancelar'          Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${CANCELAR_EXCLUSAO}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='icone I'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Detalhes"]

Quando clicar na lixeira de um template que tem layout publicado
    Sleep                                                                        2s
    Click Link                                                                   xpath=//a[contains(text()," Utilizado na automação")]/../../td/a[@title="Excluir"]

Quando preencher o formulário sem informar um nome e clicar em Salvar
    Sleep                                                                        2s
    Select From List By Label                                                    ${SELECT_CLIENTE}                                                                            Utilizado na automação
    Click Element                                                                ${BOTAO_SALVAR}

Quando preencher o formulário sem informar um cliente e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_NOME}                                                                                Teste Automação QA
    Click Element                                                                ${BOTAO_SALVAR}

Quando preencher todo formulário e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_NOME}                                                                                Teste Automação QA
    Select From List By Label                                                    ${SELECT_CLIENTE}                                                                            Utilizado na automação
    Click Element                                                                ${BOTAO_SALVAR}

Quando clicar em Salvar com o campo nome vazio
    Sleep                                                                        2s
    Clear Element Text                                                           ${INPUT_NOME}
    Click Element                                                                ${BOTAO_SALVAR}

Quando clicar em Salvar com o campo cliente vazio
    Sleep                                                                        2s
    Select From List By Label                                                    ${SELECT_CLIENTE}                                                                            (Selecione)
    Click Element                                                                ${BOTAO_SALVAR}

Quando alterar o nome do template e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_NOME}                                                                                Teste Automação QA Alterado
    Click Element                                                                ${BOTAO_SALVAR}

Quando marcar o check e clicar em Excluir
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_CONFIRM}
    Click Element                                                                ${EXCLUIR_CLIENTE}

Quando desmarcar o checkbox ativo e clicar em Salvar
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_SITUACAO}
    Click Element                                                                ${BOTAO_SALVAR}

Quando marcar o checkbox ativo e clicar em Salvar
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_SITUACAO}
    Click Element                                                                ${BOTAO_SALVAR}

Quando filtrar por Inativo e clicar em Atualizar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_FILTRO}                                                                              Inativo
    Click Element                                                                ${BOTAO_ATUALIZAR}

Então deve ser exibido o template inativo
    Sleep                                                                        2s
    Page Should Contain Element                                                  xpath=//a[contains(text()," Teste Automação QA")]
    Page Should Contain Element                                                  xpath=//a[contains(text()," Teste Automação QA")]/../../td[4][contains(text(),"Inativo")]

Então deve ser exibida a tela "${TELA}"
    Run Keyword If                                                               '${TELA}'=='Templates'                                                                       Page Should Contain Element          xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Detalhes da template'    Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Detalhes do cliente'     Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Editar cliente'          Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Excluir template'        Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]

Então deve ser exibido validações do formulário
    Element Should Be Visible                                                    id=Data_Name-error
    Element Should Be Visible                                                    id=Data_CustomerId-error

Então deve ser exibido a validação de "${VALIDACAO}"
    Run Keyword If                                                               '${VALIDACAO}'=='nome'                                                                       Element Should Be Visible            id=Data_Name-error
    ...                                                                          ELSE IF                                                                                      '${VALIDACAO}'=='cliente'            Element Should Be Visible                                                                                id=Data_CustomerId-error

Então deve ser exibida uma mensagem informando que não é possível excluir
    Page Should Contain Element                                                  xpath=//li[contains(text(),"Excluir template")]
    Page Should Contain                                                          Não é possível excluir este registro.