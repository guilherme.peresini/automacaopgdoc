*** Settings ***
Resource                ../resource/Resource.robot
Test SETUP              Abrir navegador
Test TEARDOWN           Fechar navegador

***Variables ***
#LOCATORS TELA CLIENTES
${BOTAO_INCLUIR}        xpath=//form[@id='lookupForm']//a[contains(text()," Incluir")]
${BOTAO_ATUALIZAR}      xpath=//form[@id='lookupForm']//button
${INPUT_FILTRO}         id=filter

#LOCATORS TELA NOVO CLIENTE
${INPUT_NOME}           id=Data_Name
${SELECT_AMBIENTE}      xpath=//div[@id="ddlgRow"]/select
${BOTAO_CANCELAR}       xpath=//form[@id='mainForm']//a[contains(text()," Cancelar")]
${BOTAO_SALVAR}         xpath=//*[@id='submitButton']
${CHECK_SITUACAO}       xpath=//*[@id="Data_IsActive"]/../label

#LOCATORS TELA DETALHES CLIENTE
${BOTAO_EDITAR}         xpath=//a[@title='Editar']
${BOTAO_EXCLUIR}        xpath=//a[@title='Excluir']
${BOTAO_VOLTAR}         xpath=//a[@title='Voltar']

#LOCATORS TELA ExCLUIR CLIENTE
${CHECK_CONFIRM}        xpath=//*[@id="confirmation"]/../label
${EXCLUIR_CLIENTE}      id=submitButton
${CANCELAR_EXCLUSAO}    xpath=//*[@id='submitButton']/../a

*** Test Cases ***
Cenário 01: Validação de cancelamento de cadastro de cliente
    Dado que estou na tela de Novo cliente
    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Clientes"

Cenário 02: Validação de gravação de cliente sem preencher o formulário
    Dado que estou na tela de Novo cliente
    Quando clicar em "Salvar"
    Então deve ser exibido uma validação do formulário

Cenário 03: Validação de gravação de cliente preenchendo todo formulário
    Dado que estou na tela de Novo cliente
    Quando preencher o formulário e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 04: Validação de acesso à edição de cliente pela página Detalhes do cliente
    Dado que estou na tela Detalhes do cliente
    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar cliente"

Cenário 05: Validação de acesso à exclusão de cliente pela página Detalhes do cliente
    Dado que estou na tela Detalhes do cliente
    Quando clicar em "Excluir"
    Então deve ser exibida a tela "Excluir cliente"

Cenário 06: Validação de voltar pela página Detalhes do cliente
    Dado que estou na tela Detalhes do cliente
    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Clientes"

Cenário 07: Validação de acesso à edição de cliente pela página Cliente
    Dado que estou na tela Cliente
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

Cenário 08: Validação de acesso à exclusão de cliente pela página Cliente
    Dado que estou na tela Cliente
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

Cenário 09: Validação de acesso à detalhes do cliente pela página Cliente
    Dado que estou na tela Cliente
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 10: Validação de edição de cliente
    Dado que estou na tela Cliente
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando alterar o nome do cliente e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 11: Validação de edição de cliente com nome vazio
    Dado que estou na tela Cliente
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando clicar em Salvar com o campo nome vazio
    Então deve ser exibido uma validação do formulário

Cenário 12: Validação de cancelamento de exclusão de cliente
    Dado que estou na tela Cliente
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

    Quando clicar em "X Cancelar"
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 13: Validação de bloqueio de cliente
    Dado que estou na tela Cliente
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando desmarcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 14: Validação de desbloqueio de cliente
    Dado que estou na tela Cliente
    Quando filtrar por Inativo e clicar em Atualizar
    Então deve ser exibido o cliente inativo

    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar cliente"

    Quando marcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes do cliente"

Cenário 15: Validação de exclusão de cliente
    Dado que estou na tela Cliente
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir cliente"

    Quando marcar o check e clicar em Excluir
    Então deve ser exibida a tela "Clientes"

Cenário 16: Validação de exclusão de cliente vinculado à um template
    Dado que estou na tela Cliente
    Quando clicar em lixeira com cliente vinculado à um template
    Então deve ser exibida uma mensagem informando que não é possível excluir

*** Keywords ***
Dado que estou na tela de Novo cliente
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Clientes
    Sleep                                                                        2s
    Click Link                                                                   ${BOTAO_INCLUIR}
    Wait Until Page Contains Element                                             xpath=//li[contains(text(),"Novo cliente")]                                                  10s

Dado que estou na tela Detalhes do cliente
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Clientes
    Sleep                                                                        2s
    Click Link                                                                   xpath=//a[contains(text()," Teste Automação QA")]
    Wait Until Page Contains Element                                             xpath=//li[contains(text(),"Detalhes do cliente")]                                           10s

Dado que estou na tela Cliente
    Realizar login                                                               ${LOGIN.username}                                                                            ${LOGIN.senha}
    Sleep                                                                        2s
    Acessar menu                                                                 Clientes

Quando marcar o check e clicar em Excluir
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_CONFIRM}
    Click Element                                                                ${EXCLUIR_CLIENTE}

Quando clicar em "${ELEMENTO}"
    Run Keyword If                                                               '${ELEMENTO}'=='Cancelar'                                                                    Run Keywords                        Sleep                                                                                                    2s                                        AND    Click Link    ${BOTAO_CANCELAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Salvar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Element        ${BOTAO_SALVAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Editar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EDITAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Excluir'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EXCLUIR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Voltar'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_VOLTAR}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='Icone com lápis'    Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Editar"]
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='lixeira'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Excluir"]
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='X Cancelar'         Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${CANCELAR_EXCLUSAO}
    ...                                                                          ELSE IF                                                                                      '${ELEMENTO}'=='icone I'            Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Detalhes"]

Quando clicar em lixeira com cliente vinculado à um template
    Sleep                                                                        2s
    Click Link                                                                   xpath=//a[contains(text()," Utilizado na automação")]/../../td/a[@title="Excluir"]

Quando preencher o formulário e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_NOME}                                                                                Teste Automação QA
    Select From List By Label                                                    ${SELECT_AMBIENTE}                                                                           Homologação (homologação)
    Click Element                                                                ${BOTAO_SALVAR}

Quando alterar o nome do cliente e clicar em Salvar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_NOME}                                                                                Teste Automação QA alterado
    Select From List By Label                                                    ${SELECT_AMBIENTE}                                                                           Homologação (homologação)
    Click Element                                                                ${BOTAO_SALVAR}

Quando clicar em Salvar com o campo nome vazio
    Sleep                                                                        2s
    Clear Element Text                                                           ${INPUT_NOME}
    Select From List By Label                                                    ${SELECT_AMBIENTE}                                                                           Homologação (homologação)
    Click Element                                                                ${BOTAO_SALVAR}

Quando desmarcar o checkbox ativo e clicar em Salvar
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_SITUACAO}
    Click Element                                                                ${BOTAO_SALVAR}

Quando marcar o checkbox ativo e clicar em Salvar
    Sleep                                                                        2s
    Click Element                                                                ${CHECK_SITUACAO}
    Click Element                                                                ${BOTAO_SALVAR}

Quando filtrar por Inativo e clicar em Atualizar
    Sleep                                                                        2s
    Input Text                                                                   ${INPUT_FILTRO}                                                                              Inativo
    Click Element                                                                ${BOTAO_ATUALIZAR}

Então deve ser exibida a tela "${TELA}"
    Run Keyword If                                                               '${TELA}'=='Clientes'                                                                        Page Should Contain Element         xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Detalhes do cliente'    Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Editar cliente'         Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                          ELSE IF                                                                                      '${TELA}'=='Excluir cliente'        Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]

Então deve ser exibido uma validação do formulário
    Element Should Be Visible                                                    id=Data_Name-error

Então deve ser exibido o cliente inativo
    Page Should Contain Element                                                  xpath=//a[contains(text()," Teste Automação QA")]
    Page Should Contain Element                                                  xpath=//a[contains(text()," Teste Automação QA")]/../../td[3][contains(text(),"Inativo")]

Então deve ser exibida uma mensagem informando que não é possível excluir
    Page Should Contain Element                                                  xpath=//li[contains(text(),"Excluir cliente")]
    Page Should Contain                                                          Não é possível excluir este registro.