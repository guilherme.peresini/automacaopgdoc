*** Settings ***
Resource               ../resource/Resource.robot
Test SETUP             Abrir navegador
Test TEARDOWN          Fechar navegador

***Variables ***
${CAMPO_EMAIL}         id=Email
${CAMPO_SENHA}         id=Password
${BOTAO_PROSSEGUIR}    id=submitButton

${ERROR_EMAIL}         id=Email-error
${ERROR_SENHA}         id=Password-error
${ERROR}               xpath=//form[@id="mainForm"]//span[contains(text(),"Email ou senha inválida.")]

&{LOGIN}               username=user@pgmais.com.br                                                        senha=123

*** Test Cases ***
Cenário 01: Validação de login válido
    Quando preencher o formulário de login com informações válidas
    Então deve ser realizado o login

Cenário 02: Validação de login sem informar email
    Quando preencher o formulário de login sem informar "email"
    Então deve ser exibido uma validação do campo "email"

Cenário 03: Validação de login sem informar senha
    Quando preencher o formulário de login sem informar "senha"
    Então deve ser exibido uma validação do campo "senha"

Cenário 04: Validação de login sem informar nenhum campo
    Quando clicar em prosseguir sem preencher o formulário
    Então deve ser exibido uma validação para os campos

Cenário 05: Validação de login informando login inválido
    Quando preencher o formulário de login com login inválido
    Então deve ser exibido uma mensagem de validação

Cenário 06: Validação de login informando senha inválida
    Quando preencher o formulário de login com senha inválida
    Então deve ser exibido uma mensagem de validação

*** Keywords ***
Quando preencher o formulário de login com informações válidas
    Input Text                                                        ${CAMPO_EMAIL}                                     ${LOGIN.username}
    Input Text                                                        ${CAMPO_SENHA}                                     ${LOGIN.senha}
    Click Element                                                     ${BOTAO_PROSSEGUIR}

Quando preencher o formulário de login com login inválido
    Input Text                                                        ${CAMPO_EMAIL}                                     Login_inválido
    Input Text                                                        ${CAMPO_SENHA}                                     ${LOGIN.senha}
    Click Element                                                     ${BOTAO_PROSSEGUIR}

Quando preencher o formulário de login com senha inválida
    Input Text                                                        ${CAMPO_EMAIL}                                     ${LOGIN.username}
    Input Text                                                        ${CAMPO_SENHA}                                     senha_inválida
    Click Element                                                     ${BOTAO_PROSSEGUIR}

Quando preencher o formulário de login sem informar "${CAMPO}"
    Run Keyword If                                                    '${CAMPO}'=='email'                                Run Keywords                 Clear Element Text           ${CAMPO_EMAIL}    AND                  Input Text    ${CAMPO_SENHA}    ${LOGIN.senha}
    ...                                                               ELSE IF                                            '${CAMPO}'=='senha'          Input Text                   ${CAMPO_EMAIL}    ${LOGIN.username}
    Click Element                                                     ${BOTAO_PROSSEGUIR}

Quando clicar em prosseguir sem preencher o formulário
    Clear Element Text                                                ${CAMPO_EMAIL}
    Click Element                                                     ${BOTAO_PROSSEGUIR}

Então deve ser exibido uma mensagem de validação
    Element Should Be Visible                                         ${ERROR}

Então deve ser exibido uma validação para os campos
    Element Should Be Visible                                         ${ERROR_EMAIL}
    Element Should Be Visible                                         ${ERROR_SENHA}

Então deve ser exibido uma validação do campo "${CAMPO}"
    Run Keyword If                                                    '${CAMPO}'=='email'                                Element Should Be Visible    ${ERROR_EMAIL}
    ...                                                               ELSE IF                                            '${CAMPO}'=='senha'          Element Should Be Visible    ${ERROR_SENHA}

Então deve ser realizado o login
    Wait Until Page Contains Element                                  xpath=//h3[contains(text(),'Última atividade')]    10s
    Page Should Contain Element                                       xpath=//h3[contains(text(),'Última atividade')]