*** Settings ***
Resource                  ../resource/Resource.robot
Test SETUP                Abrir navegador
Test TEARDOWN             Fechar navegador

*** Variables ***
#LOCATORS TELA AMBIENTES
${BOTAO_NOVO_AMBIENTE}    xpath=//form[@id='lookupForm']//a[contains(text()," Novo ambiente")]

#LOCATORS TELA NOVO AMBIENTE
${BOTAO_CANCELAR}         xpath=//form[@id='mainForm']//a[contains(text()," Cancelar")]
${INPUT_NOME}             id=Data_Name
${INPUT_URL}              id=Data_Url
${CHECK_URL}              xpath=//label[contains(text(),'Validar a url antes de salvar')]
${CHECK_ATIVO}            xpath=//label[contains(text(),'Ativo')]
${VALIDACAO_NOME}         id=Data_Name-error
${VALIDACAO_URL}          id=Data_Url-error
${BOTAO_VOLTAR}           xpath=//a[contains(text(),"Voltar")]
${BOTAO_SALVAR}           xpath=//*[@id='submitButton']

#LOCATORS TELA DETALHES AMBIENTE
${BOTAO_EDITAR}           xpath=//a[contains(text(),"Editar")]
${BOTAO_EXCLUIR}          xpath=//a[contains(text(),"Excluir")]
${CHECK_CONFIRM}          xpath=//*[@id="confirmation"]/../label
${EXCLUIR}                id=submitButton

*** Test Cases ***
Cenário 01: Validação de cancelamento de cadastro de ambiente
    Dado que estou na tela de Novo ambiente
    Quando clicar em "Cancelar"
    Então deve ser exibida a tela "Ambientes"

Cenário 02: Validação de gravação de ambiente sem preencher o campo nome
    Dado que estou na tela de Novo ambiente
    Quando preencher o formulário com exceção do campo nome e clicar em Salvar
    Então deve ser exibido validação do campo "Nome"

Cenário 03: Validação de gravação de ambiente sem preencher o campo URL
    Dado que estou na tela de Novo ambiente
    Quando preencher o formulário com exceção do campo URL e clicar em Salvar
    Então deve ser exibido validação do campo "URL"

Cenário 04: Validação do Voltar na tela Novo ambiente
    Dado que estou na tela de Novo ambiente
    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Ambientes"

Cenário 05: Validação de cadastro de ambiente
    Dado que estou na tela de Novo ambiente
    Quando preencher todo o formulário e clicar em salvar
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 06: Validação de acesso à Detalhes do ambiente pela página ambientes
    Dado que estou na tela Ambientes
    Quando clicar em "icone I"
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 07: Validação de acesso à Editar ambiente pela página ambientes
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

Cenário 08: Validação de acesso à Excluir ambiente pela página ambientes
    Dado que estou na tela Ambientes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir ambiente"

Cenário 09: Validação do Voltar na tela Detalhes do ambiente
    Dado que estou na tela Detalhes do ambiente
    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Ambientes"

Cenário 10: Validação de acesso à Editar ambiente pela página Detalhes do ambiente
    Dado que estou na tela Detalhes do ambiente
    Quando clicar em "Editar"
    Então deve ser exibida a tela "Editar ambiente"

Cenário 11: Validação de acesso à Excluir ambiente pela página Detalhes do ambiente
    Dado que estou na tela Detalhes do ambiente
    Quando clicar em "Excluir"
    Então deve ser exibida a tela "Excluir ambiente"

Cenário 12: Validação de alteração de cadastro ambiente sem informar o campo nome
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando clicar em salvar com o campo nome vazio
    Então deve ser exibido validação do campo "Nome"

Cenário 13: Validação de alteração de cadastro ambiente sem informar o campo URL
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando clicar em salvar com o campo URL vazio
    Então deve ser exibido validação do campo "URL"

Cenário 14: Validação do Voltar na tela de Editar ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 15: Validação do Cancelar na tela de Editar ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando clicar em "X Cancelar"
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 16: Validação de alteração de cadastro de ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando alterar o nome do ambiente e clicar em salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 17: Validação de bloqueio de cadastro de ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando desmarcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 18: Validação de desbloqueio de cadastro de ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "Icone com lápis"
    Então deve ser exibida a tela "Editar ambiente"

    Quando marcar o checkbox ativo e clicar em Salvar
    Então deve ser exibida a tela "Detalhes da template"

Cenário 19: Validação do Voltar na tela de Excluir ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir ambiente"

    Quando clicar em "Voltar"
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 20: Validação do Cancelar na tela de Excluir ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir ambiente"

    Quando clicar em "X Cancelar"
    Então deve ser exibida a tela "Detalhes do ambiente"

Cenário 21: Validação de exclusão de cadastro de ambiente
    Dado que estou na tela Ambientes
    Quando clicar em "lixeira"
    Então deve ser exibida a tela "Excluir ambiente"

    Quando marcar o check e clicar em Excluir
    Então deve ser exibida a tela "Ambientes"

*** Keywords ***
Dado que estou na tela de Novo ambiente
    Realizar login                                                                ${LOGIN.username}                                                                  ${LOGIN.senha}
    Sleep                                                                         2s
    Execute JavaScript    window.scrollTo(${0},${500})
    Acessar menu                                                                  Ambientes
    Sleep                                                                         2s
    Click Link                                                                    ${BOTAO_NOVO_AMBIENTE}
    Wait Until Page Contains Element                                              xpath=//li[contains(text(),"Novo ambiente")]                                       10s

Dado que estou na tela Ambientes
    Realizar login                                                                ${LOGIN.username}                                                                  ${LOGIN.senha}
    Sleep                                                                         2s
    Execute JavaScript    window.scrollTo(${0},${500})
    Acessar menu                                                                  Ambientes

Dado que estou na tela Detalhes do ambiente
    Realizar login                                                                ${LOGIN.username}                                                                  ${LOGIN.senha}
    Sleep                                                                         2s
    Execute JavaScript    window.scrollTo(${0},${500})
    Acessar menu                                                                  Ambientes
    Sleep                                                                         2s
    Click Link                                                                    xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Detalhes"]


Quando clicar em "${ELEMENTO}"
    Run Keyword If                                                                '${ELEMENTO}'=='Cancelar'                                                          Run Keywords                         Sleep                                                                                                    2s                                        AND    Click Link    ${BOTAO_CANCELAR}
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='Voltar'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_VOLTAR}
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='Editar'              Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EDITAR}
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='Excluir'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_EXCLUIR}
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='Icone com lápis'     Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Editar"]
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='lixeira'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Excluir"]
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='X Cancelar'          Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           ${BOTAO_CANCELAR}
    ...                                                                           ELSE IF                                                                            '${ELEMENTO}'=='icone I'             Run Keywords                                                                                             Sleep                                     2s     AND           Click Link           xpath=//a[contains(text()," Teste Automação QA")]/../../td/a[@title="Detalhes"]

Quando preencher o formulário com exceção do campo nome e clicar em Salvar
    Sleep                                                                         2s
    Input Text                                                                    ${INPUT_URL}                                                                       https://www.google.com.br
    Click Element                                                                 ${CHECK_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando preencher o formulário com exceção do campo URL e clicar em Salvar
    Sleep                                                                         2s
    Input Text                                                                    ${INPUT_NOME}                                                                      Teste Automação QA
    Click Element                                                                 ${CHECK_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando preencher todo o formulário e clicar em salvar
    Sleep                                                                         2s
    Input Text                                                                    ${INPUT_NOME}                                                                      Teste Automação QA
    Input Text                                                                    ${INPUT_URL}                                                                       https://www.google.com.br
    Click Element                                                                 ${CHECK_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando clicar em salvar com o campo nome vazio
    Sleep                                                                         2s
    Clear Element Text                                                            ${INPUT_NOME}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando clicar em salvar com o campo URL vazio
    Sleep                                                                         2s
    Clear Element Text                                                            ${INPUT_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando alterar o nome do ambiente e clicar em salvar
    Sleep                                                                         2s
    Input Text                                                                    ${INPUT_NOME}                                                                      Teste Automação QA Alterado
    Click Element                                                                 ${BOTAO_SALVAR}

Quando desmarcar o checkbox ativo e clicar em Salvar
    Sleep                                                                         2s
    Click Element                                                                 ${CHECK_ATIVO}
    Click Element                                                                 ${CHECK_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando marcar o checkbox ativo e clicar em Salvar
    Sleep                                                                         2s
    Click Element                                                                 ${CHECK_ATIVO}
    Click Element                                                                 ${CHECK_URL}
    Click Element                                                                 ${BOTAO_SALVAR}

Quando marcar o check e clicar em Excluir
    Sleep                                                                         2s
    Click Element                                                                 ${CHECK_CONFIRM}
    Click Element                                                                 ${EXCLUIR}

Então deve ser exibido validação do campo "${CAMPO}"
    Run Keyword If                                                                '${CAMPO}'=='Nome'                                                                 Element Should Be Visible            ${VALIDACAO_NOME}
    ...                                                                           ELSE IF                                                                            '${CAMPO}'=='URL'                    Element Should Be Visible                                                                                ${VALIDACAO_URL}

Então deve ser exibida a tela "${TELA}"
    Run Keyword If                                                                '${TELA}'=='Ambientes'                                                             Page Should Contain Element          xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${TELA}")]
    ...                                                                           ELSE IF                                                                            '${TELA}'=='Detalhes do ambiente'    Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                           ELSE IF                                                                            '${TELA}'=='Editar ambiente'         Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]
    ...                                                                           ELSE IF                                                                            '${TELA}'=='Excluir ambiente'        Page Should Contain Element                                                                              xpath=//li[contains(text(),"${TELA}")]