*** Settings ***
Resource         ../resource/Resource.robot
Test SETUP       Abrir navegador
Test TEARDOWN    Fechar navegador

*** Test Cases ***
Cenário 01: Validação de acesso ao menu Clientes
    Dado que estou na dashboard
    Quando clicar em "Clientes"
    Então deve ser exibido a página "Clientes"

Cenário 02: Validação de acesso ao menu Templates
    Dado que estou na dashboard
    Quando clicar em "Templates"
    Então deve ser exibido a página "Templates"

Cenário 03: Validação de acesso ao menu Componentes
    Dado que estou na dashboard
    Quando clicar em "Componentes"
    Então deve ser exibido a página "Componentes"

Cenário 04: Validação de acesso ao menu Tags
    Dado que estou na dashboard
    Quando clicar em "Tags"
    Então deve ser exibido a página "Tags"

Cenário 05: Validação de acesso ao menu Ambientes
    Dado que estou na dashboard
    Quando clicar em Ambientes
    Então deve ser exibido a página "Ambientes"

Cenário 06: Validação de acesso ao menu Usuários
    Dado que estou na dashboard
    Quando clicar em Usuários
    Então deve ser exibido a página "Usuários"

Cenário 07: Validação de Logoff
    Dado que estou na dashboard
    Quando clicar em logoff
    Então deve ser exibida a tela de login

*** Keywords ***
Dado que estou na dashboard
    Realizar login                                   ${LOGIN.username}                                                                                          ${LOGIN.senha}

Quando clicar em Ambientes
    Execute JavaScript    window.scrollTo(${0},${500})
    Acessar menu                                     Ambientes

Quando clicar em Usuários
    Execute JavaScript    window.scrollTo(${0},${500})
    Acessar menu                                     Usuários

Quando clicar em "${MENU}"
    Acessar menu                                     ${MENU}

Quando clicar em logoff
    Sleep                                            2s
    Mouse Down On Link                               xpath=//a[@title='Efetuar logoff']
    Click Element                                    xpath=//a[@title='Efetuar logoff']

Então deve ser exibido a página "${ACESSO}"
    Sleep                                            2s
    Page Should Contain Element                      xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${ACESSO}")]

Então deve ser exibida a tela de login
    Sleep                                            2s
    Title Should Be                                  Login - PG Doc Designer