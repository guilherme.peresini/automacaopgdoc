*** Settings ***
Library               SeleniumLibrary
Library               String
Library               OperatingSystem
Library               FakerLibrary
Library               DateTime
Library               String

*** Variables ***
${BROWSER}            chrome
${URL}                https://hml-docdesigner.pgmais.io

${CAMPO_EMAIL}        id=Email
${CAMPO_SENHA}        id=Password
${BOTAO_PROSSEGUIR}   id=submitButton


&{LOGIN}              username=user@pgmais.com.br                            senha=123

*** Keywords ***
Abrir navegador
   Open Browser                                                           ${URL}              ${BROWSER}
   
Fechar navegador
    Close Browser

Fechar navegador e aplicar fatal erro em caso de falha
    Close Browser
    Run Keyword If Test Failed              Fatal Error

Realizar login
    [Arguments]                         ${LOGIN.username}                                                    ${LOGIN.senha}
    Input Text                          ${CAMPO_EMAIL}                                                       ${LOGIN.username}
    Input Text                          ${CAMPO_SENHA}                                                       ${LOGIN.senha}
    Click Element                       ${BOTAO_PROSSEGUIR}
    Wait Until Element Is Visible       xpath=//h3[contains(text(),'Última atividade')]                      10s

Acessar menu
    [Arguments]                         ${MENU}
    Sleep                               2s
    Mouse Down On Link                  xpath=//ul[@id="sidebarnav"]//span[contains(text(),'${MENU}')]/..
    Click Element                       xpath=//ul[@id="sidebarnav"]//span[contains(text(),'${MENU}')]/..
    Wait Until Page Contains Element    xpath=//div[@id="page-content-wrapper"]//div[@class="row page-titles"]//a[contains(text(),"${MENU}")]

Gerar nome randomico
    ${CLIENTE}=                         Name
    [Return]                            ${CLIENTE}

Obter data atual
    ${DATA}=                            Get Current Date
    [Return]                            ${DATA}

Acrescentar dias à data atual
    [Arguments]                         ${DIAS}
    ${DATA}=                            Obter data atual
    ${DATA}=                            Add Time To Date                                                     ${DATA}                                                                ${DIAS}
    ${DATA}=                            Convert Date                                                         ${DATA}                                                                result_format=%d/%m/%Y
    [Return]                            ${DATA}